string fourmerComposition(string in){
	in.erase(remove(in.begin(), in.end(), '\n'), in.end());
	int occurrences[256];
	int fourmerInd = 0;
	string ret = "";
	for (int i = 0; i < 256; i++){
		occurrences[i] = 0;
	}

	for (int i = 0; i < in.length() - 3; i++){
		fourmerInd = fourmerToIndex(in.substr(i, i + 4));
		occurrences[fourmerInd]++;
	}

	for (int i = 0; i < 256; i++){
		ret += to_string(occurrences[i]) + ' ';
	}
	return ret;
}

int fourmerToIndex(string fourmer){
	int ret = 0;
	ret += nucleotideToIndex(fourmer.at(0)) * 64;
	ret += nucleotideToIndex(fourmer.at(1)) * 16;
	ret += nucleotideToIndex(fourmer.at(2)) * 4;
	ret += nucleotideToIndex(fourmer.at(3));
	return ret;
}

int nucleotideToIndex(char nucleotide){
	if (nucleotide == 'A'){
		return 0;
	}
	if (nucleotide == 'C'){
		return 1;
	}
	if (nucleotide == 'G'){
		return 2;
	}
	if (nucleotide == 'T'){
		return 3;
	}
}
