#include "printFncts.h"
using namespace std;

int TestNo = 1;

void printVector(vector<int> vec){
	printf("{");
	for (vector<int>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("%d, ", *it);
	}
	printf("}");
}

void printStringVector(vector<string> vec){
	printf("\n");
	for (vector<string>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("%s\n", (*it).c_str());
	}
	printf("\n");
}

void printSuffixVector(vector<suffix> vec){
	printf("\n");
	for (vector<suffix>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("index: %d rank1: %d rank2 %d\n", (*it).index, (*it).rank[0], (*it).rank[1]);
	}
	printf("\n");
}

void printSuffixVectorAndText(vector<suffix> vec, string text){
	printf("\n");
	for (vector<suffix>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("index: %d suffix: %s\n", (*it).index, text.substr((*it).index, text.size() - (*it).index).c_str());
	}
	printf("\n");
}

void printCountNucleotidesTest(string in){
	string out = countNucleotides(in);

	printf("\nTestNo: %d\nInput: %s\nOutput:%s", TestNo, in.c_str(), out.c_str());
	TestNo++;
}

void printTranscribeTest(string in){
	string out = transcribe(in);

	printf("\nTestNo: %d\nInput: \n%s\nOutput:\n%s", TestNo, in.c_str(), out.c_str());

	printToFile(out);

	TestNo++;
}

void printToFile(string in){
	ofstream out("output.txt");
	out << in;
	out.close();
}

void printFourmerCompositionTest(string in){
	string out = fourmerComposition(in);

	printf("\nTestNo: %d\nInput: \n%s\nOutput:\n%s", TestNo, in.c_str(), out.c_str());

	printToFile(out);

	TestNo++;
}

void printFindOccurrencesTest(string inPattern, string inGenome){
	string out = findOccurrences(inPattern, inGenome);

	printf("\nTestNo: %d\ninPattern: \n%s\ninGenome: \n%s\nOutput:\n%s", TestNo, inPattern.c_str(), inGenome.c_str(), out.c_str());

	printToFile(out);

	TestNo++;
}

void printFindMostFrequentWordsTest(int inK, string inText){
	string out = findMostFrequentWords(inK, inText);

	printf("\nTestNo: %d\ninK: %d\ninText: \n%s\nOutput:\n%s\n", TestNo, inK, inText.c_str(), out.c_str());

	printToFile(out);

	TestNo++;
}

void printSpliceAndTranslateTest(vector<string> inDnaAndIntrons){
	string out = spliceAndTranslate(inDnaAndIntrons);
	printf("\nTestNo: %d\ninDnaAndIntrons: \n", TestNo);
	printStringVector(inDnaAndIntrons);

	printf("Output:\n%s\n", out.c_str());

	printToFile(out);
}

void printSharedKmersTest(int inK, string inStr1, string inStr2){
	string out = sharedKmers(inK, inStr1, inStr2);
	printf("\nTestNo: %d\ninK: %d\ninStr1: \n%s\ninStr2: \n%s\nOutput:\n%s\n", TestNo, inK, inStr1.c_str(), inStr2.c_str(), out.c_str());

	printToFile(out);
}

void printConstDeBruijnTest(vector<string> kmers){
	string out = constDeBruijn(kmers);
	printf("\nTestNo: %d\ninKmers: \n", TestNo);
	printStringVector(kmers);

	printf("Output:\n%s\n", out.c_str());

	printToFile(out);
}

void printMaxNonBranchingPathsTest(vector<string> adjList){
	string out = maxNonBranchingPaths(adjList);
	printf("\nTestNo: %d\ninAdjList: \n", TestNo);
	printStringVector(adjList);

	printf("Output:\n%s\n", out.c_str());

	printToFile(out);
}

void printEulerPathTest(vector<string> adjList){
	string out = eulerPath(adjList);
	printf("\nTestNo: %d\ninAdjList: \n", TestNo);
	printStringVector(adjList);

	printf("Output:\n%s\n", out.c_str());

	printToFile(out);
}

void printGraph(vector<Node *> graph){
	Node * curNode = NULL;
	Node * curChild = NULL;
	for (vector<Node *>::iterator it = graph.begin(); it != graph.end(); ++it){
		curNode = (*it);
		if (curNode->ins > 0 || curNode->children.size() > 0){
			printf("%d -> ", curNode->val);
			for (vector<Node *>::iterator childIt = curNode->children.begin(); childIt != curNode->children.end(); ++childIt){
				curChild = (*childIt);
				printf("%d, ", curChild->val);
			}
			printf("\n");
		}

	}
}

void printTriePreorder(TrieNode * root){
	for (int i = 0; i < 4; i++){
		if (root->children[i] != NULL){
			printf("%d->%d:%c\n", root->val, root->children[i]->val, indexToNucleotide(i));
			printTriePreorder(root->children[i]);
		}
	}
}

void printTrieConstructionTest(vector<string> adjList){
	string out = trieConstruction(adjList);
	printf("\nTestNo: %d\ninPatterns: \n", TestNo);
	printStringVector(adjList);

	printf("Output:\n%s\n", out.c_str());

	printToFile(out);
}

void printTrieMatchingTest(vector<string> textAndPatterns){
	string out = trieMatching(textAndPatterns);
	printf("\nTestNo: %d\ninTextAndPatterns: \n", TestNo);
	printStringVector(textAndPatterns);

	printf("Output:\n%s\n", out.c_str());

	printToFile(out);
}

void printSuffixArrayTest(string text){
	string out = suffixArray(text);
	printf("\nTestNo: %d\ninText: %s\n", TestNo, text.c_str());

	printf("Output:\n%s\n", out.c_str());

	printToFile(out);
}

void printSuffixArrayMatchingTest(vector<string> textAndPatterns){
	string out = suffixArrayMatching(textAndPatterns);
	printf("\nTestNo: %d\ninTextAndPatterns: \n", TestNo);
	printStringVector(textAndPatterns);

	printf("Output:\n%s\n", out.c_str());

	printToFile(out);
}