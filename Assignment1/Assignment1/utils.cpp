#include "utils.h"
using namespace std;

string fastaToString(string in){
	ifstream infile(in);
	string ret;
	string line;
	getline(infile, line);
	while (getline(infile, line)){
		ret += line;
	}
	return ret;
}

vector<string> fastaToVec(string in){
	ifstream infile(in);
	vector<string> ret = vector<string>();
	string line = "";
	string temp = "";

	getline(infile, line);
	while (getline(infile, line)){
		if (line.at(0) == '>'){
			ret.push_back(temp);
			temp = "";
		}
		else{
			temp += line;
		}
	}
	ret.push_back(temp);

	return ret;
}

vector<string> fileToVec(string in){
	ifstream infile(in);
	vector<string> ret = vector<string>();
	string line = "";

	while (getline(infile, line)){
		ret.push_back(line);
	}

	return ret;
}

int nucleotideToIndex(char nucleotide){
	if (nucleotide == 'A'){
		return 0;
	}
	if (nucleotide == 'C'){
		return 1;
	}
	if (nucleotide == 'G'){
		return 2;
	}
	if (nucleotide == 'T'){
		return 3;
	}
	if (nucleotide == '$'){
		return -1;
	}
	return 0;
}

char indexToNucleotide(int ind){
	if (ind == 0){
		return 'A';
	}
	if (ind == 1){
		return 'C';
	}
	if (ind == 2){
		return 'G';
	}
	if (ind == 3){
		return 'T';
	}
	return 'A';
}

int kmerToIndex(string kmer){
	int ret = 0;

	for (int i = 0; i < kmer.size(); i++){
		ret += nucleotideToIndex(kmer.at(i)) * pow(4,(kmer.size() - i - 1));
	}
	return ret;
}

unsigned long long int kmerToLongIndex(string kmer){
	unsigned long long int ret = 0;

	for (unsigned long long int i = 0; i < kmer.size(); i++){
		ret += nucleotideToIndex(kmer.at(i)) * pow(4, (kmer.size() - i - 1));
	}
	return ret;
}

string indexToKmer(int index, int k){
	string ret = "";
	int ithCharInd = 0;
	for (;k > 0; k--){
		ithCharInd = floor(index / pow(4, k - 1));
		ret += indexToNucleotide(ithCharInd);
		index -= ithCharInd*pow(4, k - 1);
	}
	return ret;
}

char codonToAminoAcid(string codon){
	if (codon.size() < 3){
		return 'A';
	}
	//GC*
	if (codon.at(0) == 'G' && codon.at(1) == 'C'){
		return 'A';
	}
	//CG*, AG(A||G)
	if ((codon.at(0) == 'C' && codon.at(1) == 'G') ||
		(codon.at(0) == 'A' && codon.at(1) == 'G' && 
		(codon.at(2) == 'A' || codon.at(2) == 'G'))){
		return 'R';
	}
	if ((codon.at(0) == 'A' && codon.at(1) == 'A' &&
		(codon.at(2) == 'T' || codon.at(2) == 'C'))){
		return 'N';
	}
	if ((codon.at(0) == 'G' && codon.at(1) == 'A' &&
		(codon.at(2) == 'T' || codon.at(2) == 'C'))){
		return 'D';
	}
	if ((codon.at(0) == 'T' && codon.at(1) == 'G' &&
		(codon.at(2) == 'T' || codon.at(2) == 'C'))){
		return 'C';
	}
	if ((codon.at(0) == 'C' && codon.at(1) == 'A' &&
		(codon.at(2) == 'A' || codon.at(2) == 'G'))){
		return 'Q';
	}
	if ((codon.at(0) == 'G' && codon.at(1) == 'A' &&
		(codon.at(2) == 'A' || codon.at(2) == 'G'))){
		return 'E';
	}
	if (codon.at(0) == 'G' && codon.at(1) == 'G'){
		return 'G';
	}
	if ((codon.at(0) == 'C' && codon.at(1) == 'A' &&
		(codon.at(2) == 'T' || codon.at(2) == 'C'))){
		return 'H';
	}
	if (codon.at(0) == 'A' && codon.at(1) == 'T' && codon.at(2) != 'G'){
		return 'I';
	}
	if ((codon.at(0) == 'C' && codon.at(1) == 'T') ||
		(codon.at(0) == 'T' && codon.at(1) == 'T' &&
		(codon.at(2) == 'A' || codon.at(2) == 'G'))){
		return 'L';
	}
	if ((codon.at(0) == 'A' && codon.at(1) == 'A' &&
		(codon.at(2) == 'A' || codon.at(2) == 'G'))){
		return 'K';
	}
	if (codon.at(0) == 'A' && codon.at(1) == 'T' && codon.at(2) == 'G'){
		return 'M';
	}
	if ((codon.at(0) == 'T' && codon.at(1) == 'T' &&
		(codon.at(2) == 'T' || codon.at(2) == 'C'))){
		return 'F';
	}
	if (codon.at(0) == 'C' && codon.at(1) == 'C'){
		return 'P';
	}
	if ((codon.at(0) == 'T' && codon.at(1) == 'C') ||
		(codon.at(0) == 'A' && codon.at(1) == 'G' &&
		(codon.at(2) == 'T' || codon.at(2) == 'C'))){
		return 'S';
	}
	if (codon.at(0) == 'A' && codon.at(1) == 'C'){
		return 'T';
	}
	if (codon.at(0) == 'T' && codon.at(1) == 'G' && codon.at(2) == 'G'){
		return 'W';
	}
	if ((codon.at(0) == 'T' && codon.at(1) == 'A' &&
		(codon.at(2) == 'T' || codon.at(2) == 'C'))){
		return 'Y';
	}
	if (codon.at(0) == 'G' && codon.at(1) == 'T'){
		return 'V';
	}
	return 'A';
}

bool startOrStopCodon(string codon){
	if (//(codon.at(0) == 'A' && codon.at(1) == 'T' && codon.at(2) == 'G') ||
		(codon.at(0) == 'T' && codon.at(1) == 'G' && codon.at(2) == 'A') ||
		(codon.at(0) == 'T' && codon.at(1) == 'A' &&
		(codon.at(2) == 'A' || codon.at(2) == 'G'))){
		return true;
	}
	return false;
}

string translate(string dna){
	string ret = "";
	for (int i = 0; i < dna.size() - 2; i += 3){
		if (startOrStopCodon(dna.substr(i, 3))){
			continue;
		}
		ret += codonToAminoAcid(dna.substr(i, 3));
	}
	return ret;
}

vector<int> findOccurrenceIndices(string pattern, string genome){
	vector<int> ret = vector<int>();
	int j = 0;
	for (int i = 0; i < genome.size() - pattern.size() - 1; i++){
		for (j = 0; j < pattern.size(); j++){
			if (genome.at(i + j) != pattern.at(j)){
				j = 0;
				break;
			}
		}
		if (j == pattern.size()){
			ret.push_back(i);
			i = i + j;
		}
	}

	return ret;
}

string splice(vector<string> dnaAndIntrons){
	string dna = dnaAndIntrons.at(0);
	string intron = "";
	string temp = "";
	vector<int> occurrences;

	vector<string>::iterator it = dnaAndIntrons.begin();
	if (it == dnaAndIntrons.end()){
		return dna;
	}
	++it;
	for (; it != dnaAndIntrons.end(); ++it){
		intron = *it;

		occurrences = findOccurrenceIndices(intron, dna);
		int i = 0;
		temp = "";
		for (vector<int>::iterator occIt = occurrences.begin(); occIt != occurrences.end(); ++occIt){
			temp += dna.substr(i, (*occIt - i));
			i = *occIt + intron.size();
		}
		temp += dna.substr(i, dna.size() - i);
		dna = temp;
	}

	return dna;
}

string complement(string in){
	string ret = "";
	for (int i = 0; i < in.size(); i++){
		if (in.at(in.size() - i - 1) == 'A'){
			ret += 'T';
		}
		else if (in.at(in.size() - i - 1) == 'C'){
			ret += 'G';
		}
		else if (in.at(in.size() - i - 1) == 'G'){
			ret += 'C';
		}
		else if (in.at(in.size() - i - 1) == 'T'){
			ret += 'A';
		}
	}
	return ret;
}

int compare(suffix a, suffix b){
	if (a.rank[0] != b.rank[0]){
		if (a.rank[0] < b.rank[0]){
			return 1;
		}
		return 0;
	}
	else{
		if (a.rank[1] < b.rank[1]){
			return 1;
		}
		return 0;
	}
}

vector<int> makeSuffixArray(string text){
	int size = text.length();
	vector<int> ret;
	ret.resize(size);
	vector<suffix> suffixes;
	suffixes.resize(size);
	vector<int> indexes;
	indexes.resize(size);
	int rank = 0;
	int prevRank = 0;
	int nextIndex = 0;


	for (int i = 0; i < size; i++){
		suffixes.at(i).index = i;
		suffixes.at(i).rank[0] = nucleotideToIndex(text[i]);
		if ((i + 1) < size){
			suffixes.at(i).rank[1] = nucleotideToIndex(text[i + 1]);
		}
		else{
			suffixes.at(i).rank[1] = -1;
		}
	}

	sort(suffixes.begin(), suffixes.end(), compare);

	//suffixes now sorted by first 2 characters
	//now sort by first 4, 8 etc (k)
	for (int k = 4; k < 2 * size; k = k * 2){
		//initialize first suffix to rank 0
		rank = 0;
		prevRank = suffixes.at(0).rank[0];
		suffixes.at(0).rank[0] = rank;
		indexes.at(suffixes.at(0).index) = 0;

		//assign ranks, if first and second ranks are the same, give same rank
		//else increment rank by one
		for (int i = 1; i < size; i++){

			if (suffixes.at(i).rank[0] == prevRank &&
				suffixes.at(i).rank[1] == suffixes.at(i - 1).rank[1]){
				prevRank = suffixes.at(i).rank[0];
				suffixes.at(i).rank[0] = rank;
			}
			else{
				prevRank = suffixes.at(i).rank[0];
				suffixes.at(i).rank[0] = ++rank;
			}
			//update indexes with the new rank
			indexes.at(suffixes.at(i).index) = i;

		}
		//assign rank[1] to each suffix based on rank of next suffix
		for (int i = 0; i < size; i++){
			nextIndex = suffixes.at(i).index + k / 2;
			if (nextIndex < size){
				suffixes.at(i).rank[1] = suffixes.at(indexes.at(nextIndex)).rank[0];
			}
			else{
				suffixes.at(i).rank[1] = -1;
			}
		}

		sort(suffixes.begin(), suffixes.end(), compare);

	}

	for (int i = 0; i < size; i++){
		ret.at(i) = suffixes.at(i).index;
	}
	return ret;
}