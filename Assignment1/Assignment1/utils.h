#ifndef UTILS_H
#define UTILS_H

#include "printFncts.h"

//Utilities
std::string fastaToString(std::string);
std::vector<std::string> fastaToVec(std::string in);
std::vector<std::string> fileToVec(std::string in);

int nucleotideToIndex(char);
char indexToNucleotide(int ind);
int kmerToIndex(std::string kmer);
unsigned long long int kmerToLongIndex(std::string kmer);
std::string indexToKmer(int index, int k);

char codonToAminoAcid(std::string codon);
std::string translate(std::string dna);

std::vector<int> findOccurrenceIndices(std::string pattern, std::string genome);
std::string splice(std::vector<std::string> dnaAndIntrons);

std::string complement(std::string in);

int compare(suffix a, suffix b);
std::vector<int> makeSuffixArray(std::string text);

#endif //UTILS_H