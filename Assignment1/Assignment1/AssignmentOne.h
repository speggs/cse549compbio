#ifndef ASSIGNMENTONE_H
#define ASSIGNMENTONE_H

#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <vector>
#include <cstddef> 
#include <stack> 

class Node {
public:
	int val;
	int ins;
	bool visited;
	std::vector<Node*> children;

	Node() {}

	Node(int _val, std::vector<Node*> _children) {
		val = _val;
		children = _children;
		ins = 0;
		visited = false;
	}
};

class TrieNode {
public:
	struct TrieNode * children[4];
	bool endOfWord;
	int val;
	int count;

	TrieNode(int _val) {
		bool endOfWord = false;
		for (int i = 0; i < 4; i++){
			children[i] = NULL;
		}
		val = _val;
	}
};

struct suffix{
	int index;
	int rank[2];
};

#include "printFncts.h"
#include "utils.h"


std::string countNucleotides(std::string);

std::string transcribe(std::string);

std::string fourmerComposition(std::string);

std::string findOccurrences(std::string pattern, std::string genome);

std::string findMostFrequentWords(int k, std::string text);

std::string spliceAndTranslate(std::vector<std::string> dnaAndIntrons);

std::string sharedKmers(int k, std::string str1, std::string str2);

std::vector<std::string> makeSusrc(std::vector<std::string> kmers);
std::string constDeBruijn(std::vector<std::string> kmers);

std::string maxNonBranchingPaths(std::vector<std::string> adjList);

std::string eulerPath(std::vector<std::string> adjList);

std::string trieConstruction(std::vector<std::string> patterns);

std::string trieMatching(std::vector<std::string> textAndPatterns);

std::string suffixArray(std::string text);

std::string suffixArrayMatching(std::vector<std::string> textAndPatters);

#endif //ASSIGNMENTONE_H