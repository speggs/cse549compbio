#ifndef PRINTFNCTS_H
#define PRINTFNCTS_H

#include <string>
#include <fstream>
#include <stdio.h>
#include <iostream>

#include "AssignmentOne.h"

//Printers
//Tests
void printCountNucleotidesTest(std::string in);
void printTranscribeTest(std::string in);
void printFourmerCompositionTest(std::string in);
void printFindOccurrencesTest(std::string inPattern, std::string inGenome);
void printFindMostFrequentWordsTest(int inK, std::string inText);
void printSpliceAndTranslateTest(std::vector<std::string> inDnaAndIntrons);
void printSharedKmersTest(int inK, std::string inStr1, std::string inStr2);

void printConstDeBruijnTest(std::vector<std::string> kmers);
void printMaxNonBranchingPathsTest(std::vector<std::string> adjList);
void printEulerPathTest(std::vector<std::string> adjList);
void printTrieConstructionTest(std::vector<std::string> adjList);
void printTrieMatchingTest(std::vector<std::string> adjList);
void printSuffixArrayTest(std::string text);
void printSuffixArrayMatchingTest(std::vector<std::string> textAndPatterns);

void printVector(std::vector<int> vec);
void printStringVector(std::vector<std::string> vec); 
void printSuffixVector(std::vector<suffix> vec);
void printSuffixVectorAndText(std::vector<suffix> vec, std::string text);
void printToFile(std::string in);
void printGraph(std::vector<Node *> graph);
void printTriePreorder(TrieNode * root);

#endif //PRINTFNCTS_H