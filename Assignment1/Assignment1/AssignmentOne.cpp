#include "AssignmentOne.h"
using namespace std;

string countNucleotides(string in){
	int a = 0, c = 0, g = 0, t = 0;
	string ret = "";
	for (int i = 0; i < in.length(); i++){
		if (in.at(i) == 'a' || in.at(i) == 'A'){
			a++;
		}if (in.at(i) == 'c' || in.at(i) == 'C'){
			c++;
		}if (in.at(i) == 'g' || in.at(i) == 'G'){
			g++;
		}if (in.at(i) == 't' || in.at(i) == 'T'){
			t++;
		}
	}
	ret += to_string(a) + ' ';
	ret += to_string(c) + ' ';
	ret += to_string(g) + ' ';
	ret += to_string(t);
	return ret;
}

string transcribe(string in){
	string ret = string(in);

	for (int i = 0; i < in.length(); i++){
		if (in.at(i) == 'T'){
			ret.at(i) = 'U';
		}
	}

	return ret;
}

int fourmerToIndex(string fourmer){
	int ret = 0;
	ret += nucleotideToIndex(fourmer.at(0)) * 64;
	ret += nucleotideToIndex(fourmer.at(1)) * 16;
	ret += nucleotideToIndex(fourmer.at(2)) * 4;
	ret += nucleotideToIndex(fourmer.at(3));
	return ret;
}

string fourmerComposition(string in){
	in.erase(remove(in.begin(), in.end(), '\n'), in.end());
	int occurrences[256];
	int fourmerInd = 0;
	string ret = "";
	for (int i = 0; i < 256; i++){
		occurrences[i] = 0;
	}

	for (int i = 0; i < in.length() - 3; i++){
		fourmerInd = fourmerToIndex(in.substr(i, i + 4));
		occurrences[fourmerInd]++;
	}

	for (int i = 0; i < 256; i++){
		ret += to_string(occurrences[i]) + ' ';
	}
	return ret;
}

string findOccurrences(string pattern, string genome){
	string ret = "";
	int j = 0;
	for (int i = 0; i < genome.size() - pattern.size() - 1; i++){
		for (j = 0; j < pattern.size(); j++){
			if (genome.at(i + j) != pattern.at(j)){
				j = 0;
				break;
			}
		}
		if (j == pattern.size()){
			ret += to_string(i) + ' ';
		}
	}

	return ret;
}

string findMostFrequentWords(int k, string text){
	vector<int> occurrences = vector<int>();
	string ret = "";
	int maxOccurrences = 0;
	for (int i = 0; i < pow(4, k); i++){
		occurrences.push_back(0);
	}
	
	for (int i = 0; i < text.size() - k; i++){
		occurrences.at(kmerToIndex(text.substr(i, k)))++;
	}
	for (vector<int>::iterator it = occurrences.begin(); it != occurrences.end(); ++it){
		if (maxOccurrences < *it){
			maxOccurrences = *it;
		}
	}

	int ind = 0;
	for (vector<int>::iterator it = occurrences.begin(); it != occurrences.end(); ++it){
		if (*it == maxOccurrences){
			ret += indexToKmer(ind, k) + ' ';
		}
		ind++;
	}
	return ret;
}

string spliceAndTranslate(vector<string> dnaAndIntrons){
	string ret = "";
	string spliced = "";

	spliced = splice(dnaAndIntrons);

	ret = translate(spliced);
	return ret;
}


string sharedKmers(int k, string str1, string str2){
	string ret = "";
	unsigned long long int kmerInd = 0;
	unordered_map<unsigned long long int, vector<unsigned long long int>> kmerIndicesMap;
	vector<unsigned long long int> temp;
	string comp;
	string kmer;

	for (unsigned long long int i = 0; i < str1.size() - k + 1; i++){
		kmerInd = kmerToLongIndex(str1.substr(i, k));
		if (kmerIndicesMap.count(kmerInd) == 0){
			temp = vector<unsigned long long int>();
			kmerIndicesMap[kmerInd] = temp;
		}
		kmerIndicesMap[kmerInd].push_back(i);
	}

	for (unsigned long long int i = 0; i < str2.size() - k + 1; i++){
		kmer = str2.substr(i, k);
		kmerInd = kmerToLongIndex(kmer);

		if (kmerIndicesMap.count(kmerInd) > 0){
			temp = kmerIndicesMap[kmerInd];
			for (vector<unsigned long long int>::iterator it = temp.begin(); it != temp.end(); ++it){
				ret += '(' + to_string(*it) + ", " + to_string(i) + ")\n";
			}
		}

		comp = complement(kmer);
		kmerInd = kmerToLongIndex(comp);
		if (kmerIndicesMap.count(kmerInd) > 0){
			temp = kmerIndicesMap[kmerInd];
			for (vector<unsigned long long int>::iterator it = temp.begin(); it != temp.end(); ++it){
				ret += '(' + to_string(*it) + ", " + to_string(i) + ")\n";
			}
		}
	}

	return ret;
}

vector<string> makeSusrc(vector<string> kmers){
	unordered_set<string> kmerSet = unordered_set<string>();
	vector<string> ret = vector<string>();
	string temp = "";
	for (vector<string>::iterator it = kmers.begin(); it != kmers.end(); ++it){
		if (kmerSet.count(*it) == 0){
			kmerSet.emplace(*it);
			ret.push_back(*it);
		}
		temp = complement(*it);
		if (kmerSet.count(temp) == 0){
			kmerSet.emplace(temp);
			ret.push_back(temp);
		}
	}

	return ret;
}

string constDeBruijn(vector<string> kmers){
	string ret = "";
	vector<string> susrc = makeSusrc(kmers);
	sort(susrc.begin(), susrc.end());
	for (vector<string>::iterator it = susrc.begin(); it != susrc.end(); ++it){
		ret += "(" + (*it).substr(0, (*it).length() - 1) + ", " + (*it).substr(1, (*it).length() - 1) + ")\n";
	}
	return ret;
}

vector<Node *> makeGraph(vector<string> adjList){
	vector<Node *> ret = vector<Node *>();
	int size = 0;
	vector<string>::iterator it = adjList.end();
	--it;
	string last = *it;
	string sizeStr = last.substr(0, last.find_first_of(' '));
	size = stoi(sizeStr);
	Node * newNode = NULL;
	string fromStr = "";
	int from = 0;
	string toStr = "";
	int to = 0;
	size_t lastComma = string::npos;
	size_t prevLastComma = string::npos;
	Node * curNode = NULL;
	string temp = "";

	for (int i = 0; i <= size; i++){
		newNode = new Node(i, vector<Node *>());
		ret.push_back(newNode);
	}

	for (it = adjList.begin(); it != adjList.end(); ++it){
		fromStr = (*it).substr(0, (*it).find_first_of(' '));
		from = stoi(fromStr);
		toStr = (*it).substr((*it).find_last_of('>')+2, (*it).length());
		curNode = ret.at(from);

		lastComma = toStr.find_last_of(',');
		prevLastComma = toStr.length();
		while (lastComma != string::npos){
			temp = toStr.substr(lastComma + 1, prevLastComma);
			to = stoi(temp);
			curNode->children.push_back(ret.at(to));
			ret.at(to)->ins++;
			prevLastComma = lastComma;
			lastComma = toStr.find_last_of(',', lastComma - 1);
		}
		to = stoi(toStr.substr(0, prevLastComma));
		curNode->children.push_back(ret.at(to));
		ret.at(to)->ins++;
	}

	return ret;
}

bool is1in1out(Node * node){
	return (node->children.size() == 1 && node->ins == 1);
}

string maxNonBranchingPaths(vector<string> adjList){
	vector<Node *> graph = makeGraph(adjList);
	Node * curRoot = NULL;
	Node * curNode = NULL;
	string paths = "";

	for (vector<Node *>::iterator it = graph.begin(); it != graph.end(); ++it){
		curRoot = (*it);
		if (!is1in1out(curRoot) && curRoot->children.size() > 0){
			curRoot->visited = true;
			for (vector<Node *>::iterator childIt = curRoot->children.begin(); childIt != curRoot->children.end(); ++childIt){
				curNode = (*childIt);
				curNode->visited = true;
				paths += to_string(curRoot->val) + " -> " + to_string(curNode->val);
				while (is1in1out(curNode)){
					curNode = curNode->children.at(0);
					curNode->visited = true;
					paths += " -> " + to_string(curNode->val);
				}
				paths += "\n";
			}
		}
	}
	for (vector<Node *>::iterator it = graph.begin(); it != graph.end(); ++it){
		curRoot = (*it);
		if (!curRoot->visited && is1in1out(curRoot)){
			curNode = curRoot->children.at(0);
			curNode->visited = true;
			paths += to_string(curRoot->val) + " -> " + to_string(curNode->val);
			while (curNode != curRoot && is1in1out(curNode)){
				curNode = curNode->children.at(0);
				curNode->visited = true;
				paths += " -> " + to_string(curNode->val);
			}
			paths += "\n";
		}
	}
	return paths;
}

vector<Node *> makeGraphUnordered(vector<string> adjList){
	vector<Node *> ret = vector<Node *>();
	int size = 0;
	string sizeStr = "";
	size = 0;
	Node * newNode = NULL;
	string fromStr = "";
	int from = 0;
	string toStr = "";
	int to = 0;
	size_t lastComma = string::npos;
	size_t prevLastComma = string::npos;
	Node * curNode = NULL;
	string temp = "";

	for (vector<string>::iterator it = adjList.begin(); it != adjList.end(); ++it){
		sizeStr = (*it).substr(0, (*it).find_first_of(' '));
		if (size < stoi(sizeStr)){
			size = stoi(sizeStr);
		}
	}

	for (int i = 0; i <= size; i++){
		newNode = new Node(i, vector<Node *>());
		ret.push_back(newNode);
	}

	for (vector<string>::iterator it = adjList.begin(); it != adjList.end(); ++it){
		fromStr = (*it).substr(0, (*it).find_first_of(' '));
		from = stoi(fromStr);
		toStr = (*it).substr((*it).find_last_of('>') + 2, (*it).length());
		curNode = ret.at(from);

		lastComma = toStr.find_last_of(',');
		prevLastComma = toStr.length();
		while (lastComma != string::npos){
			temp = toStr.substr(lastComma + 1, prevLastComma);
			to = stoi(temp);
			curNode->children.push_back(ret.at(to));
			ret.at(to)->ins++;
			prevLastComma = lastComma;
			lastComma = toStr.find_last_of(',', lastComma - 1);
		}
		to = stoi(toStr.substr(0, prevLastComma));
		curNode->children.push_back(ret.at(to));
		ret.at(to)->ins++;
	}

	return ret;
}

string eulerPath(vector<string> adjList){
	vector<Node *> graph = makeGraphUnordered(adjList);
	Node * curNode = NULL;
	string path = "";
	Node * aNode = NULL;
	Node * bNode = NULL;
	Node * startNode = NULL;
	Node * nextNode = NULL;

	unordered_map<int, int> edgeCount;
	stack<int> curPath;
	vector<int> circuit;

	for (vector<Node *>::iterator it = graph.begin(); it != graph.end(); ++it){
		curNode = (*it);
		edgeCount[curNode->val] = curNode->children.size();
		if (curNode->ins > 0 || curNode->children.size() > 0){
			if (startNode == NULL){
				startNode = curNode;
			}
			if (curNode->ins + 1 == curNode->children.size()){
				aNode = curNode;
				if (bNode != NULL){
				}
			}
			if (curNode->ins == curNode->children.size() + 1){
				bNode = curNode;
				if (aNode != NULL){
				}
			}
		}
	}


	if (aNode != NULL && bNode != NULL){
		//bNode->children.push_back(aNode);
		//edgeCount[bNode->val] = bNode->children.size();
		startNode = aNode;
	}

	printGraph(graph);

	curPath.push(startNode->val);
	curNode = startNode;

	int edges = 0;

	while (!curPath.empty()){
		edges = edgeCount[curNode -> val];
		if (edgeCount[curNode->val] > 0){
			curPath.push(curNode->val);
			
			nextNode = curNode->children.back();

			curNode->children.pop_back();
			edgeCount[curNode->val]--;
			
			curNode = nextNode;
		}
		else{
			circuit.push_back(curNode->val);

			curNode = graph.at(curPath.top());
			curPath.pop();

		}
	}

	for (int i = circuit.size() - 1; i >= 0; i--){
		path += to_string(circuit[i]);
		if (i){
			path += "->";
		}
	}

	return path;
}

void insertIntoTrie(TrieNode * root, string pattern){
	TrieNode * curNode = root;
	int index = 0;
	for (int i = 0; i < pattern.length(); i++){
		index = nucleotideToIndex(pattern[i]);
		if (!curNode->children[index]){
			curNode->children[index] = new TrieNode(root->count + 1);
			root->count++;
		}
		curNode = curNode->children[index];

	}
	curNode->endOfWord = true;
}

string trieToStringPreorder(TrieNode * root){
	string ret = "";
	for (int i = 0; i < 4; i++){
		if (root->children[i] != NULL){
			ret += to_string(root->val) + "->" + to_string(root->children[i]->val) + ":" + indexToNucleotide(i) + "\n";
			ret += trieToStringPreorder(root->children[i]);
		}
	}
	return ret;
}

string trieConstruction(vector<string> patterns){
	string ret = "";
	TrieNode * root = new TrieNode(0);
	root->count = 0;

	for (vector<string>::iterator it = patterns.begin(); it != patterns.end(); ++it){
		insertIntoTrie(root, (*it));
	}

	ret = trieToStringPreorder(root);

	return ret;
}

bool isLeaf(TrieNode * node){
	for (int i = 0; i < 4; i++){
		if (node->children[i] != NULL){
			return false;
		}
	}
	return true;
}

bool prefixTrieMatching(string text, TrieNode * root){
	int i = 0;
	char symbol = text[i];
	TrieNode * curNode = root;
	string path = "";

	while(true){
		if (isLeaf(curNode)){
			return true;
		}
		else if (curNode->children[nucleotideToIndex(symbol)] != NULL){
			curNode = curNode->children[nucleotideToIndex(symbol)];
			i++;
			if (i >= text.length()){
				return false;
			}
			symbol = text[i];
		}
		else{
			return false;
		}
	}
}

string trieMatching(vector<string> textAndPatterns){
	string text = textAndPatterns.at(0);
	TrieNode * root = new TrieNode(0);
	root->count = 0;
	string temp = text;
	int index = -1;
	string ret = "";
	
	vector<string>::iterator it = textAndPatterns.begin();
	++it;

	for (; it != textAndPatterns.end(); ++it){
		insertIntoTrie(root, (*it));
	}

	for (int i = 0; i < text.length(); i++){
		temp = text.substr(i, text.length());
		if (prefixTrieMatching(temp, root)){
			ret += to_string(i) + " ";
		}
	}
	return ret;
}

string suffixArray(string text){
	string ret = "";
	const int size = text.length();
	vector<suffix> suffixes;
	suffixes.resize(size);
	vector<int> indexes;
	indexes.resize(size);
	int rank = 0;
	int prevRank = 0;
	int nextIndex = 0;


	for (int i = 0; i < size; i++){
		suffixes.at(i).index = i;
		suffixes.at(i).rank[0] = nucleotideToIndex(text[i]);
		if ((i + 1) < size){
			suffixes.at(i).rank[1] = nucleotideToIndex(text[i + 1]);
		}
		else{
			suffixes.at(i).rank[1] = -1;
		}
	}

	sort(suffixes.begin(), suffixes.end(), compare);

	//suffixes now sorted by first 2 characters
	//now sort by first 4, 8 etc (k)
	for (int k = 4; k < 2 * size; k = k * 2){
		//initialize first suffix to rank 0
		rank = 0;
		prevRank = suffixes.at(0).rank[0];
		suffixes.at(0).rank[0] = rank;
		indexes.at(suffixes.at(0).index) = 0;

		//assign ranks, if first and second ranks are the same, give same rank
		//else increment rank by one
		for (int i = 1; i < size; i++){

			if (suffixes.at(i).rank[0] == prevRank &&
				suffixes.at(i).rank[1] == suffixes.at(i - 1).rank[1]){
				prevRank = suffixes.at(i).rank[0];
				suffixes.at(i).rank[0] = rank;
			}
			else{
				prevRank = suffixes.at(i).rank[0];
				suffixes.at(i).rank[0] = ++rank;
			}
			//update indexes with the new rank
			indexes.at(suffixes.at(i).index) = i;

		}
		//assign rank[1] to each suffix based on rank of next suffix
		for (int i = 0; i < size; i++){
			nextIndex = suffixes.at(i).index + k / 2;
			if (nextIndex < size){
				suffixes.at(i).rank[1] = suffixes.at(indexes.at(nextIndex)).rank[0];
			}
			else{
				suffixes.at(i).rank[1] = -1;
			}
		}

		sort(suffixes.begin(), suffixes.end(), compare);

	}

	for (int i = 0; i < size; i++){
		ret += to_string(suffixes.at(i).index);
		if (i < size - 1){
			ret += ", ";
		}
	}

	return ret;
}

int searchSuffixArray(vector<int> suffixes, string text, string pattern){
	int textSize = text.size();
	int patSize = pattern.size();
	int left = 0;
	int right = textSize - 1;
	int mid = 0, result = 0;
	string suffix = "";

	while (left <= right){
		mid = left + (right - left) / 2;
		suffix = text.substr(suffixes.at(mid), pattern.size());
		result = suffix.compare(pattern);
		//result = text.compare(suffixes.at(mid), patSize, pattern);
		if (result == 0){
			return mid;
		}
		else if (result > 0){
			right = mid - 1;
		}
		else{
			left = mid + 1;
		}
	}
	return -1;
}

string suffixArrayMatching(vector<string> textAndPatterns){
	string ret = "";
	int result = -1;
	string text = textAndPatterns.at(0);
	vector<int> suffixes = makeSuffixArray(text);
	vector<int> indexes = vector<int>();
	string pattern = "";
	string suffix = "";
	int i = -1;

	vector<string>::iterator it = textAndPatterns.begin();
	++it;

	printf("suffixes:\n");
	printVector(suffixes);

	for (; it != textAndPatterns.end(); ++it){
		pattern = (*it);
		result = searchSuffixArray(suffixes, text, pattern);
		if (result != -1){
			indexes.push_back(suffixes.at(result));
			i = result - 1;
			while (i >= 0){
				suffix = text.substr(suffixes.at(i), pattern.size());
				if (suffix.compare(pattern) == 0){
					indexes.push_back(suffixes.at(i));
				}
				else{
					break;
				}
				i--;
			}
			i = result + 1;
			while (i < suffixes.size()){
				suffix = text.substr(suffixes.at(i), pattern.size());
				if (suffix.compare(pattern) == 0){
					indexes.push_back(suffixes.at(i));
				}
				else{
					break;
				}
				i++;
			}
		}
	}

	sort(indexes.begin(), indexes.end());

	for (i = 0; i < indexes.size(); i++){
		ret += to_string(indexes.at(i));
		if (i < indexes.size() - 1){
			ret += " ";
		}
	}

	return ret;
}